package AutoTestApi;

import org.junit.jupiter.api.BeforeAll;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractTest {
    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String xAuthToken;
    private static String baseUrl;

    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);
        xAuthToken=  prop.getProperty("X-Auth-Token");
        baseUrl= prop.getProperty("base_url");
    }
    public static String getXAuthToken() {
        return xAuthToken;
    }
    public static String getBaseUrl() {
        return baseUrl;
    }
}