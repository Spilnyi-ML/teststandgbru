package AutoTestApi;

import org.junit.jupiter.api.Test;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.lessThan;

public class StandGbRuTest extends AbstractTest {

    @Test
    void getMyPostsTest1() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getMyPostsTest2() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "2")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getMyPostsTest3() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getMyPostsTest4() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getMyPostsTest5() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "5")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getNotMyPostsTest1() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getNotMyPostsTest2() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getNotMyPostsTest3() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
               // .queryParam("page", "1")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getNotMyPostsTest4() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "5")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void getNotMyPostsTest5() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .queryParam("page", "20")
                .header("X-Auth-Token", getXAuthToken())
                .when()
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .statusLine(containsString("OK"))
                .time(lessThan(5000L))
                .contentType(ContentType.JSON)
                .log()
                .all()
        ;
    }

    @Test
    void authPositiveTest() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "Ivanov1998")
                .multiPart("password", "4008010555")
                .when()
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .statusCode(200)
                .log()
                .all()
        ;
    }

    @Test
    void authNegativeLoginTest() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "Ivanov199")
                .multiPart("password", "4008010555")
                .when()
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .statusCode(401)
                .log()
                .all()
        ;
    }

    @Test
    void authNegativePassTest() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "Ivanov1998")
                .multiPart("password", "40080105")
                .when()
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .statusCode(401)
                .log()
                .all()
        ;
    }
}
