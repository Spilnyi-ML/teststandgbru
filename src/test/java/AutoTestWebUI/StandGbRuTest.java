package AutoTestWebUI;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class StandGbRuTest {
    WebDriver webDriver;
    WebDriverWait webDriverWait;
    Actions actions;

    @BeforeAll
    static void registerDriver(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupBrowser(){
        webDriver = new ChromeDriver();
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        actions = new Actions(webDriver);
        webDriver.get("https://test-stand.gb.ru/login");
        }

    public boolean isElementPresent(By by)
    {
        return webDriver.findElements(by).size() > 0;
    }

    @Test
    void authPositiveTest() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("Ivanov1998");
        webDriver.findElement(By.xpath("//input[@type='password']")).sendKeys("4008010555");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
        boolean tmp = isElementPresent(By.xpath("//h1[.='Blog']"));
        Assertions.assertEquals(true, tmp);
    }

    @Test
    void authNegativeTest() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("^%$#");
        webDriver.findElement(By.xpath("//input[@type='password']")).sendKeys("4008010555");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
        boolean tmp = isElementPresent(By.xpath("//h1[.='Blog']"));
        Assertions.assertEquals(false, tmp);
    }

    @Test
    void authBoundaryValueLoginTest1() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("12");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValueLoginTest2() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("123");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValueLoginTest3() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("12345678901234567890");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValueLoginTest4() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("123456789012345678901");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValueLoginTest5() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("123456789012");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValueLoginTest6() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("^&*%");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @Test
    void authBoundaryValuePassTest() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        webDriver.findElement(By.xpath("//input[@type='password']")).sendKeys("");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        Thread.sleep(3000);
    }

    @AfterEach
    void tearDown(){
        webDriver.quit();
    }
}
