package AutoTestWebUI;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class MyPostsTest {
    WebDriver webDriver;
    WebDriverWait webDriverWait;
    Actions actions;

    @BeforeAll
    static void registerDriver(){
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupBrowser(){
        webDriver = new ChromeDriver();
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        actions = new Actions(webDriver);
        webDriver.get("https://test-stand.gb.ru/?sort=createdAt&order=ASC");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text']")));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("Ivanov1998");
        webDriver.findElement(By.xpath("//input[@type='password']")).sendKeys("4008010555");
        webDriver.findElement(By.xpath("//button[@form='login']")).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        boolean tmp = isElementPresent(By.xpath("//h1[.='Blog']"));
        Assertions.assertEquals(true, tmp);
    }

    public boolean isElementPresent(By by)
    {
        return webDriver.findElements(by).size() > 0;
    }

    @Test
    void pageNavTest() throws InterruptedException {
        boolean temp = isElementPresent(By.xpath("//a[.='Next Page']"));
        Assertions.assertEquals(true, temp);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[.='Next Page']")));
        webDriver.findElement(By.xpath("//a[.='Next Page']")).click();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[.='Previous Page']")));
        webDriver.findElement(By.xpath("//a[.='Previous Page']")).click();
    }

    @Test
    void homeButtonTest() throws InterruptedException {
        boolean temp = isElementPresent(By.xpath("//span[.='Home']"));
        Assertions.assertEquals(true, temp);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[.='Home']")));
        webDriver.findElement(By.xpath("//span[.='Home']")).click();
    }

    @Test
    void clickPostTest() throws InterruptedException {
        boolean temp = isElementPresent(By.xpath("//a[@class='post svelte-127jg4t'][1]"));
        Assertions.assertEquals(true, temp);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='post svelte-127jg4t'][1]")));
        webDriver.findElement(By.xpath("//a[@class='post svelte-127jg4t'][1]")).click();
    }

    @Test
    void postViewTest() throws InterruptedException {
        boolean temp1 = isElementPresent(By.xpath("//a[@class='post svelte-127jg4t'][1]"));
        Assertions.assertEquals(true, temp1);
        boolean temp2 = isElementPresent(By.xpath("//h2[@class='svelte-127jg4t'][1]"));
        Assertions.assertEquals(true, temp2);
        boolean temp3 = isElementPresent(By.xpath("//div[@class='description svelte-127jg4t'][1]"));
        Assertions.assertEquals(true, temp3);
    }

    @Test
    void amount10PostsPageTest() throws InterruptedException {
        boolean temp = isElementPresent(By.xpath("//a[@class='post svelte-127jg4t'][10]"));
        Assertions.assertEquals(true, temp);
    }

    @AfterEach
    void tearDown(){
        webDriver.quit();
    }
}
